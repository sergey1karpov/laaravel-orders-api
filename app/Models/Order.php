<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';

    protected $fillable = [
        'dealer_name',
        'contact_user_id',
        'credit_sum',
        'credit_term',
        'credit_percent',
        'description',
        'status',
        'bank_id',
    ];
}
