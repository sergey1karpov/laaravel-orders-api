<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Resources\OrderResource;
use App\Http\Service\OrderService;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @OA\Info(
 *     title="My API",
 *     version="1.0.0",
 *     description="This is a description of my API"
 * )
 */
class OrderController extends Controller
{
    public function __construct(private OrderService $orderService) {}

    /**
     * Создание заказа
     * @OA\Post(
     *      path="/api/v1/orders/",
     *      operationId="Order",
     *      tags={"Order"},
     *      summary="Создание заказа",
     *      @OA\Parameter(
     *            name="dealer_name",
     *            description="Название дилера",
     *            required=true,
     *            in="query",
     *            @OA\Schema(
     *                type="string"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="contact_user_id",
     *            description="Контактное лицо, id сотрудника",
     *            required=true,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="credit_sum",
     *            description="Сумма кредита, 100000.00 ",
     *            required=true,
     *            in="query",
     *      ),
     *      @OA\Parameter(
     *            name="credit_term",
     *            description="Срок кредита, кол-во месяцев",
     *            required=true,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="credit_percent",
     *            description="Процентная ставка, 10.00",
     *            required=true,
     *            in="query",
     *      ),
     *      @OA\Parameter(
     *            name="description",
     *            description="Описание причины кредита",
     *            required=true,
     *            in="query",
     *            @OA\Schema(
     *                type="string"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="bank_id",
     *            description="Связь с банком, Id",
     *            required=true,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="OK",
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="message",
     *                   type="string",
     *               ),
     *          ),
     *      )
     *  )
     * @param CreateOrderRequest $request
     * @return JsonResponse
     */
    public function createOrder(CreateOrderRequest $request): JsonResponse
    {
        $this->orderService->createNewOrder($request);

        return response()->json(['message' => 'Order create!']);
    }

    /**
     * Получение всех заказов
     * @OA\Get(
     *      path="/api/v1/orders/",
     *      operationId="GetOrders",
     *      tags={"Order"},
     *      summary="Получение всех заказов",
     *      @OA\Response(
     *          response="200",
     *          description="OK",
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="message",
     *                   type="string",
     *               ),
     *          ),
     *      )
     *  )
     * @return AnonymousResourceCollection
     */
    public function getOrders(): AnonymousResourceCollection
    {
        return OrderResource::collection(Order::paginate(10));
    }

    /**
     * Получение заказа по id
     * @OA\Get(
     *      path="/api/v1/orders/{order}",
     *      operationId="GetOrder",
     *      summary="Получение заказа по id",
     *      tags={"Order"},
     *      @OA\Parameter(
     *          name="order",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="OK",
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="message",
     *                   type="string",
     *               ),
     *          ),
     *      )
     *  )
     * @param Order $order
     * @return AnonymousResourceCollection
     */
    public function getOrder(Order $order): AnonymousResourceCollection
    {
        return OrderResource::collection(Order::findOrFail($order));
    }

    /**
     * Обновление заказа
     * @OA\Post(
     *      path="/api/v1/orders/{order}",
     *      operationId="UpdOrder",
     *      tags={"Order"},
     *      summary="Обновление заказа",
     *      @OA\Parameter(
     *           name="order",
     *           in="path",
     *           required=true,
     *           @OA\Schema(
     *               type="integer"
     *           )
     *      ),
     *      @OA\Parameter(
     *            name="dealer_name",
     *            description="Название дилера",
     *            required=false,
     *            in="query",
     *            @OA\Schema(
     *                type="string"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="contact_user_id",
     *            description="Контактное лицо, id сотрудника",
     *            required=false,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="credit_sum",
     *            description="Сумма кредита, 100000.00 ",
     *            required=false,
     *            in="query",
     *      ),
     *      @OA\Parameter(
     *            name="credit_term",
     *            description="Срок кредита, кол-во месяцев",
     *            required=false,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="credit_percent",
     *            description="Процентная ставка, 10.00",
     *            required=false,
     *            in="query",
     *      ),
     *      @OA\Parameter(
     *            name="description",
     *            description="Описание причины кредита",
     *            required=false,
     *            in="query",
     *            @OA\Schema(
     *                type="string"
     *            )
     *      ),
     *      @OA\Parameter(
     *            name="bank_id",
     *            description="Связь с банком, Id",
     *            required=false,
     *            in="query",
     *            @OA\Schema(
     *                type="integer"
     *            )
     *      ),
     *      @OA\Parameter(
     *             name="status",
     *             description="Один из вариантов: 'New', 'Process', 'Approved', 'Rejected'",
     *             required=false,
     *             in="query",
     *             @OA\Schema(
     *                 type="string"
     *             )
     *       ),
     *      @OA\Response(
     *          response="200",
     *          description="OK",
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="message",
     *                   type="string",
     *               ),
     *          ),
     *      )
     *  )
     * @param Order $order
     * @param UpdateOrderRequest $request
     * @return JsonResponse
     */
    public function updateOrder(Order $order, UpdateOrderRequest $request): JsonResponse
    {
        $this->orderService->updateOrder($order, $request);

        return response()->json(['message' => 'Order updated!']);
    }

    /**
     * Удаление
     * @OA\Delete(
     *      path="/api/v1/orders/{order}",
     *      operationId="delOrder",
     *      summary="Удаление заказа по id",
     *      tags={"Order"},
     *      @OA\Parameter(
     *          name="order",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="OK",
     *          @OA\JsonContent(
     *               @OA\Property(
     *                   property="message",
     *                   type="string",
     *               ),
     *          ),
     *      )
     *  )
     * @param Order $order
     * @return JsonResponse
     */
    public function deleteOrder(Order $order): JsonResponse
    {
        $order->delete();

        return response()->json(['message' => 'Order deleted!']);
    }
}
