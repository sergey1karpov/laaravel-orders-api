<?php

namespace App\Http\Enum;

enum OrderStatusEnum: string
{
    case NEW = 'New';
    case PROCESS = 'Process';
    case APPROVED = 'Approved';
    case REJECTED = 'Rejected';
}
