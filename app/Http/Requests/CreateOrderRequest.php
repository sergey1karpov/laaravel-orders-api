<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'dealer_name' => 'required|string|max:255',
            'contact_user_id' => 'required|integer',
            'credit_sum' => 'required|numeric|min:0|max:9999999999.99',
            'credit_term' => 'required|integer|min:1|max:999',
            'credit_percent' => 'required|numeric|min:0.0001|max:100',
            'description' => 'required|string|max:1000',
            'bank_id' => 'required|integer',
        ];
    }
}
