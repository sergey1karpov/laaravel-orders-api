<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'dealer_name' => 'sometimes|string|max:255',
            'contact_user_id' => 'sometimes|integer',
            'credit_sum' => 'sometimes|numeric|min:0|max:9999999999.99',
            'credit_term' => 'sometimes|integer|min:1|max:999',
            'credit_percent' => 'sometimes|numeric|min:0.0001|max:100',
            'description' => 'sometimes|string|max:1000',
            'bank_id' => 'sometimes|integer',
            'status' => ['sometimes', 'string', Rule::in(['New', 'Process', 'Approved', 'Rejected'])],
        ];
    }
}
