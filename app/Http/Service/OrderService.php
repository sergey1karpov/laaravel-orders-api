<?php

namespace App\Http\Service;

use App\Http\Enum\OrderStatusEnum;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;

class OrderService
{
    public function createNewOrder(CreateOrderRequest $request): void
    {
        Order::create([
            'dealer_name' => $request->dealer_name,
            'contact_user_id' => $request->contact_user_id,
            'credit_sum' => $request->credit_sum,
            'credit_term' => $request->credit_term,
            'credit_percent' => $request->credit_percent,
            'description' => $request->description,
            'status' => OrderStatusEnum::NEW->value,
            'bank_id' => $request->bank_id
        ]);
    }

    public function updateOrder(Order $order, UpdateOrderRequest $request): void
    {
        Order::where('id', $order->id)->update([
            'dealer_name' => $request->dealer_name ?? $order->dealer_name,
            'contact_user_id' => $request->contact_user_id ?? $order->contact_user_id,
            'credit_sum' => $request->credit_sum ?? $order->credit_sum,
            'credit_term' => $request->credit_term ?? $order->credit_term,
            'credit_percent' => $request->credit_percent ?? $order->credit_percent,
            'description' => $request->description ?? $order->description,
            'bank_id' => $request->bank_id ?? $order->bank_id,
            'status' => $request->status ?? $order->status,
        ]);
    }
}
