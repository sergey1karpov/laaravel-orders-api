<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'dealer_name' => $this->dealer_name,
            'contact_user_id' => $this->contact_user_id,
            'credit_sum' => $this->credit_sum,
            'credit_term' => $this->credit_term,
            'credit_percent' => $this->credit_percent,
            'description' => $this->description,
            'status' => $this->status,
            'bank_id' => $this->bank_id
        ];
    }
}
