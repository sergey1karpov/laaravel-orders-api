<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('dealer_name', 255);
            $table->unsignedBigInteger('contact_user_id');
            $table->decimal('credit_sum');
            $table->integer('credit_term');
            $table->decimal('credit_percent');
            $table->text('description');
            $table->enum('status', ['New', 'Process', 'Approved', 'Rejected'])->default('New');
            $table->unsignedBigInteger('bank_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order');
    }
};
